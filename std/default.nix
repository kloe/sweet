let
	nixpkgs = import ../nix/nixpkgs.nix;
	sweetc = import ../compiler/default.nix;
in
	nixpkgs.stdenv.mkDerivation {
		name = "sweetstd";
		src = ./.;
		buildInputs = [
			nixpkgs.ninja
			nixpkgs.perl
			sweetc
		];
		installPhase = ''
			cp -R build $out
		'';
	}
