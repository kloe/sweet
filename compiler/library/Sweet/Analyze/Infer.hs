{-# LANGUAGE TemplateHaskell #-}

-- TODO: Improve the documentation of type inference. It is a non-trivial
-- TODO: algorithm and it should be easy for people new to the type checker to
-- TODO: learn how it works.

-- | Given an expression, what is the type of this expression? This implements
-- an algorithm similar to Algorithm W, but it omits let generalization. Let
-- generalization is omitted because it introduces a lot of complexity, slows
-- down the compiler, and to be honest is only rarely used. Explicit type
-- annotations can be provided by the programmer when a polymorphic variable
-- is needed.
module Sweet.Analyze.Infer
	( -- * Infrastructure
	  Infer
	, Environment, Arity
	, State
	, Error (..)
	, runInfer
	, reportError

	  -- * Unknowns
	, freshUnknown
	, solve
	, purge

	  -- * Skolems
	, freshSkolem

	  -- * Inference
	, inferTranslationUnit
	, inferDefinition
	, inferExpression

	  -- * Unification
	, unify
	, unifyRowTypes

	  -- * Polymorphism
	, instantiate
	, skolemize
	) where

import Control.Lens ((&), (.~), (%~), (?~), (?=), (<<+=), _1, _2, at, makeLenses, use, view)
import Control.Monad (join)
import Control.Monad.RWS (RWS, runRWS)
import Data.Bitraversable (bitraverse)
import Data.Functor (void)
import Data.List (genericLength, sortBy)
import Data.List.NonEmpty (NonEmpty (..))
import Data.Map.Strict (Map)
import Data.Monoid (Dual (..))
import Data.Ord (comparing)
import Data.Semigroup ((<>))
import Data.Traversable (for)

import qualified Control.Monad.Reader as Reader
import qualified Control.Monad.Writer as Writer
import qualified Data.List.NonEmpty as NonEmpty
import qualified Data.Map.Strict as Map

import Sweet.Analyze.CollectGlobals (Globals, collectTranslationUnit, globalDomains, globalTypes, globalVars)
import Sweet.Name (Global (Intrinsic), Identifier, Local, Qualifiedness (..))
import Sweet.Syntax.Source (Definition (..), Expression (..), Kind (..), RowType (..), TranslationUnit (..), Type (..))

import qualified Sweet.Name.Intrinsic as I



-- | Monad for inference. This is a rare case of 'RWS' without any of the
-- arguments being unit:
--
--  * The reader part is used for the environment, which maps names to
--    information about them. See 'Environment'.
--  * The writer part is used to accumulate diagnostics. Multiple diagnostics
--    may be reported in a single pass. See 'reportError'.
--  * The state part is used to keep track of solved unknowns. See
--    'freshUnknown' and 'solve'.
--
-- 'Dual' is used in order to make @tell . singleton@ run in constant time; the
-- order of error messages is irrelevant.
type Infer =
	RWS Environment (Dual [Error]) State

-- | Environment for inference. Global and local variables are mapped to their
-- types; whilst global and local types are mapped to themselves (or, in the
-- case of type synonyms, to their expansions).
data Environment = Environment
	{ _envGlobals :: Globals
	, _envLocalVars :: Map Local (Type 'Q)
	, _envLocalTypes :: Map Local (Type 'Q) }

-- | How many parameters does a function or a type have?
type Arity = Word

-- | Any error that could occur during inference. These are collected by
-- 'Infer'; multiple errors may be returned from a single inference pass.
data Error
	= UnknownGlobalVar (Global 'Q) Arity
	| UnknownLocalVar Local
	| UnknownGlobalType (Global 'Q) Arity
	| UnknownLocalType Local
	| UnknownGlobalDomain (Global 'Q)
	| UnificationError (Type 'Q) (Type 'Q)
	| UnsupportedHigherRankType
	deriving stock (Show)

-- | Inference state is necessary for generating fresh unknowns, as well as
-- unifications of unknowns. Manipulate with 'freshUnknown' and 'solve'.
data State = State
	{ _stateFresh :: Word
	, _stateSolutions :: Map Word (Type 'Q) }

$(makeLenses ''Environment)
$(makeLenses ''State)

-- | Run an inference action, given a set of globals and locals.
runInfer
	:: Infer a
	-> Globals
	-> [Local]
	-> Either (NonEmpty Error) a
runInfer action globals localVars =
	let (result, _, errors) = runRWS action' env state in
	case getDual errors of
		[] -> Right result
		(e : es) -> Left (e :| es)
	where
	env = Environment globals Map.empty Map.empty
	state = State 0 Map.empty
	action' = do
		localVars' <- traverse (\l -> (,) l <$> freshUnknown) localVars
		Reader.local (envLocalVars .~ Map.fromList localVars') $
			action

class HasErrorSort e where errorSort :: e
instance HasErrorSort (Type 'Q) where errorSort = ErrorType
instance HasErrorSort (Kind 'Q) where errorSort = ErrorKind

-- | Report an error, and return the error type for convenience.
reportError :: HasErrorSort e => Error -> Infer e
reportError err = errorSort <$ Writer.tell (Dual [err])

-- | Discard result of 'reportError'.
reportError' :: Error -> Infer ()
reportError' = void . reportError @(Type 'Q)

lookupGlobalVar :: Global 'Q -> Arity -> Infer (Type 'Q)
lookupGlobalVar name arity =
	view (envGlobals . globalVars . at (name, arity)) >>= maybe failure pure
	where failure = reportError (UnknownGlobalVar name arity)

lookupLocalVar :: Local -> Infer (Type 'Q)
lookupLocalVar name =
	view (envLocalVars . at name) >>= maybe failure pure
	where failure = reportError (UnknownLocalVar name)

lookupGlobalType :: Global 'Q -> Arity -> Infer (Kind 'Q)
lookupGlobalType name arity = do
	view (envGlobals . globalTypes . at (name, arity)) >>= maybe failure pure
	where failure = reportError (UnknownGlobalType name arity)

lookupLocalType :: Local -> Infer (Type 'Q)
lookupLocalType name =
	view (envLocalTypes . at name) >>= maybe failure pure
	where failure = reportError (UnknownLocalType name)

lookupGlobalDomain :: Global 'Q -> Infer (Type 'Q)
lookupGlobalDomain name = do
	view (envGlobals . globalDomains . at name) >>= maybe failure pure
	where failure = reportError (UnknownGlobalDomain name)



-- | New unknown, not unified with anything yet.
freshUnknown :: Infer (Type 'Q)
freshUnknown = do
	idn <- stateFresh <<+= 1
	pure (UnknownType idn)

-- | From now on, the given unknown is equivalent to the given type.
solve :: Word -> Type 'Q -> Infer ()
solve idn t = stateSolutions . at idn ?= t

-- | For unknowns that are no longer unknown, resolve them using the stored
-- state.
purge :: Type 'Q -> Infer (Type 'Q)
purge t@(UnknownType idn) = use (stateSolutions . at idn) >>= maybe (pure t) purge
purge t = pure t



-- | New Skolem, different from all other types.
freshSkolem :: Infer (Type 'Q)
freshSkolem = do
	idn <- stateFresh <<+= 1
	pure (SkolemType idn)



-- | Infer the types of expressions in a translation unit.
inferTranslationUnit :: TranslationUnit Expression 'Q -> [Error]
inferTranslationUnit tu@(TranslationUnit ds) =
	let globals = collectTranslationUnit tu in
	foldMap (inferDefinition globals) ds

-- | Infer the types of expressions in a definition.
inferDefinition :: Globals -> Definition Expression 'Q -> [Error]

inferDefinition globals (NamespaceDefinition _ ds) =
	foldMap (inferDefinition globals) ds

inferDefinition _ (DomainDefinition _ _) =
	[]

inferDefinition globals (FunctionDefinition _ typeParams params retType body) =
	case runInfer action globals params' of
		Left errors -> NonEmpty.toList errors
		Right _ -> []
	where
	action :: Infer ()
 	action = do
		declaredT <- skolemize functionType
		inferredT <- inferExpression $ LambdaExpression params' body
		_ <- declaredT `unify` inferredT
		pure ()

	functionType :: Type 'Q
	functionType =
		let base = FunctionType (snd <$> params) retType in
		foldr (uncurry ForAllType) base typeParams

	params' :: [Local]
	params' = fst <$> params

-- | Infer the type of an expression.
inferExpression :: Expression 'Q -> Infer (Type 'Q)

inferExpression (LocalExpression n) =
	lookupLocalVar n

inferExpression (USVLiteralExpression _) =
	pure $ CallType (Intrinsic I.USVIntrinsic) []

inferExpression (TextLiteralExpression _) =
	pure $ CallType (Intrinsic I.TextIntrinsic) []

inferExpression (RecordLiteralExpression fs) = do
	fs' <- traverse (traverse inferExpression) fs
	let row = (rowTypeFromList (fs', RowType EmptyRowType))
	pure $ CallType (Intrinsic I.RecordIntrinsic) [row]

inferExpression (CallExpression f xs) = do
	fT <- instantiate =<< lookupGlobalVar f (genericLength xs)
	inferCallApplyExpression fT xs

inferExpression (ApplyExpression f xs) = do
	fT <- inferExpression f
	inferCallApplyExpression fT xs

inferExpression (LambdaExpression ps b) = do
	pTs <- traverse ((<$> freshUnknown) . (,)) ps
	bT <- Reader.local (envLocalVars %~ (Map.fromList pTs <>)) $
		inferExpression b
	pure $ FunctionType (snd <$> pTs) bT

inferExpression (WrapExpression d e) = do
	dT <- lookupGlobalDomain d
	eT <- inferExpression e
	dT `unify` eT
	pure $ CallType d []

inferExpression (UnwrapExpression d e) = do
	dT <- lookupGlobalDomain d
	eT <- inferExpression e
	CallType d [] `unify` eT
	pure dT

inferExpression (ProjectExpression r f) = do
	rT <- inferExpression r
	fT <- freshUnknown

	rowTailT <- freshUnknown
	let rowT = RowType (ConsRowType f fT rowTailT)

	let recordT = CallType (Intrinsic I.RecordIntrinsic) [rowT]
	recordT `unify` rT

	pure fT

inferExpression (InjectExpression field value) = do
	valueT <- inferExpression value
	rowTailT <- freshUnknown
	let rowT = RowType (ConsRowType field valueT rowTailT)
	pure $ CallType (Intrinsic I.VariantIntrinsic) [rowT]

inferExpression (MatchExpression scrutinee cases) = do
	resultT <- freshUnknown

	scrutineeT <- inferExpression scrutinee

	caseTs <- for cases $ \(field, value, body) -> do
		valueT <- freshUnknown
		Reader.local (envLocalVars %~ Map.insert value valueT) $ do
			bodyT <- inferExpression body
			ok <- resultT `unify` bodyT
			pure ((field, valueT), ok)

	let scrutineeRowT' = rowTypeFromList (fst <$> caseTs, RowType EmptyRowType)
	let scrutineeT' = CallType (Intrinsic I.VariantIntrinsic) [scrutineeRowT']
	ok <- scrutineeT `unify` scrutineeT'

	pure $ if and (ok : fmap snd caseTs)
		then resultT
		else ErrorType

inferExpression (ForeignExpression argument _source) = do
	_ <- inferExpression argument
	freshUnknown

-- | Helper function used for inferring types of call and apply expressions.
-- Takes callee function type and argument expressions.
inferCallApplyExpression :: Type 'Q -> [Expression 'Q] -> Infer (Type 'Q)
inferCallApplyExpression fT xs = do
	xTs <- traverse inferExpression xs
	result <- freshUnknown
	ok <- fT `unify` (FunctionType xTs result)
	pure (if ok then result else ErrorType)



-- | Make two types become the same, if possible. This solves unknown types if
-- necessary, and is vital in the implementation of polymorphism. If the types
-- cannot be unified this will report a type error and return 'False'.
unify :: Type 'Q -> Type 'Q -> Infer Bool
unify = go $ \failUnify -> \case
	(ErrorType, _) -> pure False
	(_, ErrorType) -> pure False

	(UnknownType idn, UnknownType idm) | idn == idm -> pure True
	(UnknownType idn, t) -> True <$ solve idn t
	(t, UnknownType idn) -> True <$ solve idn t

	(SkolemType idn, SkolemType idm) | idn == idm -> pure True
	(SkolemType _, _) -> failUnify
	(_, SkolemType _) -> failUnify

	-- Until higher-rank types are supported, these should not occur inside
	-- a type passed to unify, because they should be elimitated by
	-- 'instantiate' or 'skolemize'.
	(LocalType _, _) -> failHigherRankType
	(_, LocalType _) -> failHigherRankType

	(FunctionType ps r, FunctionType ps' r')
		| length ps /= length ps' -> failUnify
		| otherwise -> do
			oks <- sequence (zipWith unify ps ps')
			ok <- r `unify` r'
			pure (and (ok : oks))
	(FunctionType _ _, _) -> failUnify
	(_, FunctionType _ _) -> failUnify

	-- Until higher-rank types are supported, these should not occur inside
	-- a type passed to unify, because they should be elimitated by
	-- 'instantiate' or 'skolemize'.
	(ForAllType _ _ _, _) -> failHigherRankType
	(_, ForAllType _ _ _) -> failHigherRankType

	(CallType f as, CallType g bs)
		| f /= g -> failUnify
		| length as /= length bs -> failUnify
		| otherwise -> do
			_ <- lookupGlobalType f (genericLength as)
			_ <- lookupGlobalType g (genericLength bs)
			and <$> sequence (zipWith unify as bs)
	(CallType _ _, _) -> failUnify
	(_, CallType _ _) -> failUnify

	(RowType r, RowType s) -> unifyRowTypes r s

	where
	go f t u = do
		(t', u') <- join bitraverse purge (t, u)
		f (failUnify' t' u') (t', u')
	failUnify' t u = False <$ reportError' (UnificationError t u)
	failHigherRankType = False <$ reportError' UnsupportedHigherRankType

-- | A row type, fully known.
type RowTypeMap q =
	[(Identifier, Type q)]

-- | A row type, in list form.
type RowTypeList q =
	([(Identifier, Type q)], Type q)

-- | Unify two row types.
--
-- Adapted from the source code of the PureScript compiler.
unifyRowTypes :: RowType 'Q -> RowType 'Q -> Infer Bool
unifyRowTypes r1 r2 = do
	oks <- sequence matches
	ok <- uncurry unifyTails rest
	pure (and (ok : oks))
	where

	matches :: [Infer Bool]
	rest :: (RowTypeList 'Q, RowTypeList 'Q)
	(matches, rest) = alignRowTypesWith unify r1 r2

	unifyTails :: RowTypeList 'Q -> RowTypeList 'Q -> Infer Bool

	unifyTails ([], UnknownType u) (sd, r) =
		True <$ solve u (rowTypeFromList (sd, r))

	unifyTails (sd, r) ([], UnknownType u) =
		True <$ solve u (rowTypeFromList (sd, r))

	unifyTails ([], RowType EmptyRowType) ([], RowType EmptyRowType) =
		pure True

	unifyTails ([], LocalType v1) ([], LocalType v2)
		| v1 == v2 = pure True

	unifyTails ([], SkolemType s1) ([], SkolemType s2)
		| s1 == s2 = pure True

	unifyTails (sd1, UnknownType u1) (sd2, UnknownType u2) = do
		rest' <- freshUnknown
		solve u1 (rowTypeFromList (sd2, rest'))
		solve u2 (rowTypeFromList (sd1, rest'))
		pure True

	unifyTails _ _ =
		False <$ reportError' err
		where err = UnificationError (RowType r1) (RowType r2)

-- | Align two rows of types, splitting them into three parts:
--
-- * Those types which appear in both rows
-- * Those which appear only on the left
-- * Those which appear only on the right
--
-- Note: importantly, we preserve the order of the types with a given label.
--
-- Adapted from the source code of the PureScript compiler.
alignRowTypesWith
	:: forall a q
	 . (Type q -> Type q -> a)
	-> RowType q -> RowType q
	-> ([a], (RowTypeList q, RowTypeList q))
alignRowTypesWith f ty1 ty2 =
	go s1 s2
	where
	s1, s2 :: RowTypeMap q
	tail1, tail2 :: Type q
	(s1, tail1) = rowTypeToSortedList (RowType ty1)
	(s2, tail2) = rowTypeToSortedList (RowType ty2)

	go :: RowTypeMap q -> RowTypeMap q -> ([a], (RowTypeList q, RowTypeList q))
	go [] r = ([], (([], tail1), (r, tail2)))
	go r [] = ([], ((r, tail1), ([], tail2)))
	go lhs@((l1, t1) : r1) rhs@((l2, t2) : r2)
		| l1 < l2 = go r1 rhs & _2 . _1 . _1 %~ ((l1, t1) :)
		| l2 < l1 = go lhs r2 & _2 . _2 . _1 %~ ((l2, t2) :)
		| otherwise = go r1 r2 & _1 %~ (f t1 t2 :)

-- | Create a row type from a row type in list form.
--
-- Adapted from the source code of the PureScript compiler.
rowTypeFromList :: RowTypeList q -> Type q
rowTypeFromList (xs, r) =
	let step k v t = RowType (ConsRowType k v t)
	in foldr (uncurry step) r xs

-- | Create a row type in list form from a row type.
--
-- Adapted from the source code of the PureScript compiler.
rowTypeToList :: Type q -> RowTypeList q
rowTypeToList (RowType (ConsRowType name ty row)) =
	rowTypeToList row & _1 %~ ((name, ty) :)
rowTypeToList r = ([], r)

-- | Create a row type in list form from a row type, sorted by label.
--
-- Adapted from the source code of the PureScript compiler.
rowTypeToSortedList :: Type q -> RowTypeList q
rowTypeToSortedList =
	(_1 %~ sortBy (comparing fst)) . rowTypeToList



-- | Instantiate a polymorphic type, such that its type variables are replaced
-- by fresh unknowns. This happens when a name occurs in an expression.
instantiate :: Type 'Q -> Infer (Type 'Q)
instantiate = refresh freshUnknown

-- | Skolemize a polymorphic type, such that its type variables are replaced by
-- fresh Skolems. This happens when a let binding with an explicit type
-- annotation is given, or in a body of a polymorphic function.
skolemize :: Type 'Q -> Infer (Type 'Q)
skolemize = refresh freshSkolem

-- | Helper function used by both instantiate and skolemize, because those
-- functions are identical except one uses 'freshUnknown' and the other one
-- uses 'freshSkolem'.
refresh :: (Infer (Type 'Q)) -> Type 'Q -> Infer (Type 'Q)
refresh fresh (ForAllType n _ t) = do
	v <- fresh
	Reader.local (envLocalTypes . at n ?~ v) $
		refresh fresh t
refresh _ t = replaceLocalTypes t

-- | Helper function used by 'refresh'. There are some caveats:
--
--  - Local types must all be in scope. If they are not, an error is reported
--    and 'ErrorType' is returned.
--  - Higher-rank types are not yet supported. When one is encountered, this
--    will report an error and return 'ErrorType'.
replaceLocalTypes :: Type 'Q -> Infer (Type 'Q)
replaceLocalTypes ErrorType = pure ErrorType
replaceLocalTypes (UnknownType idn) = pure (UnknownType idn)
replaceLocalTypes (SkolemType idn) = pure (SkolemType idn)
replaceLocalTypes (LocalType n) = lookupLocalType n
replaceLocalTypes (FunctionType ps r) = do
	ps' <- traverse replaceLocalTypes ps
	r' <- replaceLocalTypes r
	pure $ FunctionType ps' r'
replaceLocalTypes (ForAllType _ _ _) =
	reportError UnsupportedHigherRankType
replaceLocalTypes (CallType f as) = do
	as' <- traverse replaceLocalTypes as
	pure $ CallType f as'
replaceLocalTypes (RowType EmptyRowType) =
	pure $ RowType EmptyRowType
replaceLocalTypes (RowType (ConsRowType k v t)) = do
	v' <- replaceLocalTypes v
	t' <- replaceLocalTypes t
	pure $ RowType (ConsRowType k v' t')
