-- | All sorts of names.
module Sweet.Name
	( -- * Identifiers
	  Identifier (..)

	  -- * Namespaces
	, Namespace (..)
	, rootNamespace
	, snocNamespace

	  -- * Globals
	, Qualifiedness (..)
	, Global (..)
	, weakenQualified

	  -- * Locals
	, Local (..)
	) where

import Data.Semigroup ((<>))
import Data.String (IsString (..))
import Data.Text (Text)

import Sweet.Name.Intrinsic (Intrinsic)

-- | An identifier may just be a string, but it may also be an operator.
data Identifier
	= Identifier Text
	| OperatorAsterisk
	| OperatorLessGreater
	| OperatorMinus
	| OperatorPlus
	| OperatorSlash
	deriving (Eq, Ord, Show)

instance IsString Identifier where
	fromString = Identifier . fromString

-- | Names of namespaces are lists of identifiers. The empty list represents
-- the root namespace, and is available as 'rootNamespace'. A namespace name as
-- written @std::math@ in source code is represented by @'Namespace' ["std",
-- "math"]@.
newtype Namespace =
	Namespace [Identifier]
	deriving stock (Eq, Ord, Show)

-- | The name of the root namespace, which is at the top level of any
-- translation unit. This is simply @'Namespace' []@.
rootNamespace :: Namespace
rootNamespace = Namespace []

-- | Given a namespace name, and another segment, return the name of this as a
-- nested namespace of the given namespace. For example, given @std::math@ and
-- @complex@, this will return @std::math::complex@.
snocNamespace :: Namespace -> Identifier -> Namespace
snocNamespace (Namespace ns) n = Namespace (ns <> [n])

-- | Whether a name is definitely qualified or possibly unqualified. Used to
-- index 'Global' at the type-level through data type promotion. The
-- "Sweet.Transform.Resolve" module provides functions for eliminating 'U'.
data Qualifiedness = Q | U

-- | A name of a global definition may be either qualified or unqualified. Only
-- in the first phases of compilation may names be unqualified, hence this is a
-- GADT indexed on 'Qualifiedness'.
data Global :: Qualifiedness -> * where
	Qualified :: Namespace -> Identifier -> Global q
	Intrinsic :: Intrinsic -> Global q
	Unqualified :: Identifier -> Global 'U
deriving stock instance Eq (Global q)
deriving stock instance Ord (Global q)
deriving stock instance Show (Global q)

-- | Given a qualified global, weaken its type so that it can be used where
-- possibly unqualified globals are expected.
weakenQualified :: Global q -> Global 'U
weakenQualified (Qualified ns n) = Qualified ns n
weakenQualified (Intrinsic n) = Intrinsic n
weakenQualified (Unqualified n) = Unqualified n

-- | Name of a local variable or type variable. These are always unqualified,
-- since they never refer to global definitions. Globals and locals live in
-- separate namespaces (a la Erlang), and referencing them is differentiated
-- between syntactically.
data Local
	-- | A local as given by the programmer
	= Local Identifier

	-- | A local generated during some code transformation.
	| SynthesizedLocal Word

	deriving (Eq, Ord, Show)

instance IsString Local where
	fromString = Local . fromString
