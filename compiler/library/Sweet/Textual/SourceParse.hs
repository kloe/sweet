{-# OPTIONS_GHC -Wno-name-shadowing #-}

module Sweet.Textual.SourceParse
	( -- * Infrastructure
	  parse
	, Error (..)
	, Stream (..)

	  -- * Source files
	, sourceFile

	  -- * Definitions
	, definition
	, namespaceDefinition
	, usingDefinition
	, domainDefinition
	, functionDefinition

	  -- * Names
	, identifier
	, namespace
	, global
	, local

	  -- * Expressions
	, expression
	, expression1
	, expression2
	, expression3
	, expression4

	  -- * Types
	, type_
	, type1
	, rowType

	  -- * Kinds
	, kind
	, kind1
	) where

import Control.Applicative ((<|>), many, optional)
import Control.Lens (Prism', preview)
import Data.Foldable (fold, foldl')
import Data.Functor (void)
import Data.List.NonEmpty (NonEmpty (..))
import Data.Maybe (fromMaybe)
import Data.Proxy (Proxy (..))
import Text.Megaparsec (MonadParsec)

import qualified Data.List.NonEmpty as NonEmpty
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import qualified Text.Megaparsec as Parsec

import Sweet.Textual.Token

import Sweet.Name (Qualifiedness (U))

import qualified Sweet.Name as N
import qualified Sweet.Name.Intrinsic as I
import qualified Sweet.Syntax.Source as S



parse :: String -> [(Pos, Token)] -> [S.Definition S.Expression 'U]
parse file = either (error . show) id . Parsec.parse sourceFile file . Stream

data Error =
	Error
	deriving (Eq, Ord, Show)

newtype Stream =
	Stream [(Pos, Token)]
	deriving (Eq, Ord, Show)

instance Parsec.Stream Stream where
	type Token Stream = (Pos, Token)
	type Tokens Stream = [(Pos, Token)]

	tokensToChunk Proxy = id
	chunkToTokens Proxy = id
	chunkLength Proxy = length
	chunkEmpty Proxy = null

	advance1 Proxy _ (Parsec.SourcePos file _ _) (Pos line col, _) =
		let line' = Parsec.mkPos line in
		let col' = Parsec.mkPos col in
		Parsec.SourcePos file line' col'

	advanceN Proxy _ pos [] = pos
	advanceN p t pos ts = Parsec.advance1 p t pos (last ts)

	take1_ (Stream []) = Nothing
	take1_ (Stream (t : ts)) = Just (t, Stream ts)

	takeN_ n (Stream s)
		| n <= 0 = Just ([], Stream s)
		| null s = Nothing
		| otherwise = Just (Stream <$> splitAt n s)

	takeWhile_ f (Stream s) = Stream <$> span f s



sourceFile :: MonadParsec Error Stream m => m [S.Definition S.Expression 'U]
sourceFile = many definition <* Parsec.eof



definition :: MonadParsec Error Stream m => m (S.Definition S.Expression 'U)
definition =
	namespaceDefinition <|>
	usingDefinition <|>
	domainDefinition <|>
	functionDefinition

namespaceDefinition :: MonadParsec Error Stream m => m (S.Definition S.Expression 'U)
namespaceDefinition = do
	_ <- token' KNamespace
	namespace <- identifier
	_ <- token' PBraceL
	body <- many definition
	_ <- token' PBraceR
	pure $ S.NamespaceDefinition namespace body

usingDefinition :: MonadParsec Error Stream m => m (S.Definition S.Expression 'U)
usingDefinition = do
	_ <- token' KUsing
	namespace <- namespace
	_ <- token' PParenL
	names <- identifier `Parsec.sepEndBy` token' PComma
	_ <- token' PParenR
	_ <- token' PSemicolon
	body <- many definition
	pure $ S.UsingDefinition namespace names body

domainDefinition :: MonadParsec Error Stream m => m (S.Definition S.Expression 'U)
domainDefinition = do
	_ <- token' KDomain
	name <- identifier
	_ <- token' PEquals
	underlying <- type_
	_ <- token' PSemicolon
	pure $ S.DomainDefinition name underlying

functionDefinition :: MonadParsec Error Stream m => m (S.Definition S.Expression 'U)
functionDefinition = do
	_ <- token' KFunc
	name <- identifier
	typeParameters <- optionalAngledList typeParameter
	valueParameters <- optionalParenthesizedList valueParameter
	returnType <- type_
	_ <- token' PEquals
	body <- expression
	_ <- token' PSemicolon
	pure $ S.FunctionDefinition name typeParameters valueParameters returnType body



expression :: MonadParsec Error Stream m => m (S.Expression 'U)
expression = expression1

expression1 :: forall m. MonadParsec Error Stream m => m (S.Expression 'U)
expression1 = do
	operand <- next
	suffixes <- many suffix
	pure $ foldl' (flip ($)) operand suffixes
	where
	next = expression2

	suffix :: m (S.Expression 'U -> S.Expression 'U)
	suffix =
		suffix' PAsterisk N.OperatorAsterisk <|>
		suffix' PSlash N.OperatorSlash

	suffix' :: Token -> N.Identifier -> m (S.Expression 'U -> S.Expression 'U)
	suffix' token operator = do
		_ <- token' token
		right <- next
		pure $ \left -> S.CallExpression (N.Unqualified operator) [left, right]

expression2 :: forall m. MonadParsec Error Stream m => m (S.Expression 'U)
expression2 = do
	prefixes <- many prefix
	operand <- next
	pure $ foldr ($) operand prefixes
	where
	next = expression3

	prefix :: m (S.Expression 'U -> S.Expression 'U)
	prefix =
		wrapExpressionPrefix <|>
		unwrapExpressionPrefix

	wrapExpressionPrefix :: m (S.Expression 'U -> S.Expression 'U)
	wrapExpressionPrefix = do
		_ <- token' KWrap
		domain <- global
		pure $ S.WrapExpression domain

	unwrapExpressionPrefix :: m (S.Expression 'U -> S.Expression 'U)
	unwrapExpressionPrefix = do
		_ <- token' KUnwrap
		domain <- global
		pure $ S.UnwrapExpression domain

expression3 :: forall m. MonadParsec Error Stream m => m (S.Expression 'U)
expression3 = do
	operand <- next
	suffixes <- many suffix
	pure $ foldl' (flip ($)) operand suffixes
	where
	next = expression4

	suffix, suffix' :: m (S.Expression 'U -> S.Expression 'U)
	suffix = token' PPeriod *> suffix'
	suffix' =
		applyExpressionSuffix' <|>
		projectExpressionSuffix'

	applyExpressionSuffix' :: m (S.Expression 'U -> S.Expression 'U)
	applyExpressionSuffix' = do
		arguments <- valueArgumentList
		pure $ \callee -> S.ApplyExpression callee arguments

	projectExpressionSuffix' :: m (S.Expression 'U -> S.Expression 'U)
	projectExpressionSuffix' = do
		field <- identifier
		pure $ \record -> S.ProjectExpression record field

expression4 :: forall m. MonadParsec Error Stream m => m (S.Expression 'U)
expression4 =
	localExpression <|>
	textLiteralExpression <|>
	recordLiteralExpression <|>
	callExpression <|>
	lambdaExpression <|>
	injectExpression <|>
	matchExpression <|>
	foreignExpression <|>
	doExpression <|>
	(token' PParenL *> expression <* token' PParenR)
	where
	localExpression :: m (S.Expression 'U)
	localExpression = S.LocalExpression <$> local

	textLiteralExpression :: m (S.Expression 'U)
	textLiteralExpression =
		S.TextLiteralExpression <$> token "text literal" _TextLiteral

	recordLiteralExpression :: m (S.Expression 'U)
	recordLiteralExpression = do
		_ <- token' PBraceL
		fields <- recordOrVariantField `Parsec.sepEndBy` token' PComma
		_ <- token' PBraceR
		pure $ S.RecordLiteralExpression fields

	callExpression :: m (S.Expression 'U)
	callExpression = do
		callee <- global
		arguments <- fold <$> optional valueArgumentList
		pure $ S.CallExpression callee arguments

	lambdaExpression :: m (S.Expression 'U)
	lambdaExpression = do
		_ <- token' KFunc
		parameters <- optionalParenthesizedList local
		_ <- token' PEquals
		body <- expression
		pure $ S.LambdaExpression parameters body

	injectExpression :: m (S.Expression 'U)
	injectExpression = do
		_ <- token' PBracketL
		field <- identifier
		value <- expression
		_ <- token' PBracketR
		pure $ S.InjectExpression field value

	matchExpression :: m (S.Expression 'U)
	matchExpression = do
		_ <- token' KMatch
		scrutinee <- expression
		_ <- token' PBraceL
		cases <- (`Parsec.sepEndBy` token' PComma) $ do
			_ <- token' PBracketL
			field <- identifier
			value <- local
			_ <- token' PBracketR
			body <- expression
			pure $ (field, value, body)
		_ <- token' PBraceR
		pure $ S.MatchExpression scrutinee cases

	foreignExpression :: m (S.Expression 'U)
	foreignExpression = do
		_ <- token' KForeign
		argument <- expression
		source <- token "text literal" _TextLiteral
		pure $ S.ForeignExpression argument source

	doExpression :: m (S.Expression 'U)
	doExpression = do
		_ <- token' KDo
		_ <- token' PBraceL
		result <- statement
		_ <- token' PBraceR
		pure $ result

statement :: forall m. MonadParsec Error Stream m => m (S.Expression 'U)
statement =
	bindStatement <|>
	expressionStatement
	where
	bindStatement :: m (S.Expression 'U)
	bindStatement = do
		_ <- token' KLet
		_ <- token' PBang
		name <- local
		_ <- token' PEquals
		from <- expression
		_ <- token' PSemicolon
		rest <- statement
		let rest' = S.LambdaExpression [name] rest
		pure $ S.CallExpression (N.Unqualified "bind") [from, rest']

	expressionStatement :: m (S.Expression 'U)
	expressionStatement = expression <* token' PSemicolon



type_ :: MonadParsec Error Stream m => m (S.Type 'U)
type_ = type1

type1 :: forall m. MonadParsec Error Stream m => m (S.Type 'U)
type1 =
	localType <|>
	functionType <|>
	callType <|>
	recordType <|>
	variantType
	where
	localType :: m (S.Type 'U)
	localType = S.LocalType <$> local

	functionType :: m (S.Type 'U)
	functionType = do
		_ <- token' KFunc
		parameterTypes <- optionalParenthesizedList type_
		returnType <- type_
		pure $ S.FunctionType parameterTypes returnType

	callType :: m (S.Type 'U)
	callType = do
		callee <- global
		arguments <- optionalParenthesizedList type_
		pure $ S.CallType callee arguments

	recordType :: m (S.Type 'U)
	recordType = do
		_ <- token' PBraceL
		row <- rowType
		_ <- token' PBraceR
		pure $ S.CallType (N.Intrinsic I.RecordIntrinsic) [row]

	variantType :: m (S.Type 'U)
	variantType = do
		_ <- token' PBracketL
		row <- rowType
		_ <- token' PBracketR
		pure $ S.CallType (N.Intrinsic I.VariantIntrinsic) [row]

rowType :: MonadParsec Error Stream m => m (S.Type 'U)
rowType = do
	let field = (,) <$> identifier <*> type_
	prefix <- field `Parsec.sepEndBy` token' PComma

	suffix <- optional $ token' PPipe *> type_
	let suffix' = fromMaybe (S.RowType S.EmptyRowType) suffix

	let step = flip . uncurry $ (((S.RowType .) .) .) S.ConsRowType
	pure $ foldl' step suffix' prefix



kind :: MonadParsec Error Stream m => m (S.Kind 'U)
kind = kind1

kind1 :: forall m. MonadParsec Error Stream m => m (S.Kind 'U)
kind1 =
	typeKind <|>
	rowKind
	where
	typeKind :: m (S.Kind 'U)
	typeKind = S.TypeKind <$ token' PAsterisk

	rowKind :: m (S.Kind 'U)
	rowKind = do
		_ <- token' PPercent
		_ <- token' PParenL
		element <- kind
		_ <- token' PParenR
		pure $ S.RowKind element



identifier :: MonadParsec Error Stream m => m N.Identifier
identifier = named <|> (token' KOperator *> operator)
	where
	named = N.Identifier <$> token "identifier" _Identifier
	operator =
		(N.OperatorAsterisk <$ token' PAsterisk) <|>
		(N.OperatorLessGreater <$ token' PLessGreater) <|>
		(N.OperatorMinus <$ token' PMinus) <|>
		(N.OperatorPlus <$ token' PPlus) <|>
		(N.OperatorSlash <$ token' PSlash)

namespace :: MonadParsec Error Stream m => m N.Namespace
namespace = N.Namespace <$> identifier `Parsec.sepBy1` token' PColonColon

global :: forall m. MonadParsec Error Stream m => m (N.Global 'U)
global = named <|> intrinsic
	where
	named :: m (N.Global 'U)
	named = do
		namespace <- optional . Parsec.try $ namespace <* token' PColonColon
		name <- identifier
		pure $ maybe (N.Unqualified name) (N.Qualified `flip` name) namespace

	intrinsic :: m (N.Global 'U)
	intrinsic = do
		_ <- token' PHash
		name <- token "identifier" _Identifier
		pure $ N.Intrinsic (I.intrinsics Map.! name)

local :: MonadParsec Error Stream m => m N.Local
local = token' PDollar *> fmap N.Local identifier



typeParameter :: MonadParsec Error Stream m => m (N.Local, S.Kind 'U)
typeParameter = (,) <$> local <*> kind

valueParameter :: MonadParsec Error Stream m => m (N.Local, S.Type 'U)
valueParameter = (,) <$> local <*> type_

recordOrVariantField :: MonadParsec Error Stream m => m (N.Identifier, S.Expression 'U)
recordOrVariantField = (,) <$> identifier <*> expression

valueArgumentList :: forall m. MonadParsec Error Stream m => m [(S.Expression 'U)]
valueArgumentList =
	normalArgumentList <|>
	recordArgumentList
	where
	normalArgumentList :: m [(S.Expression 'U)]
	normalArgumentList = do
		_ <- token' PParenL
		arguments <- expression `Parsec.sepEndBy` token' PComma
		_ <- token' PParenR
		pure arguments

	recordArgumentList :: m [(S.Expression 'U)]
	recordArgumentList = do
		_ <- token' PBraceL
		fields <- recordOrVariantField `Parsec.sepEndBy` token' PComma
		_ <- token' PBraceR
		pure $ [S.RecordLiteralExpression fields]



optionalAngledList :: MonadParsec Error Stream m => m a -> m [a]
optionalAngledList p = fold <$> optional go
	where
	go = token' PLess *> go' <* token' PGreater
	go' = p `Parsec.sepEndBy` token' PComma

optionalParenthesizedList :: MonadParsec Error Stream m => m a -> m [a]
optionalParenthesizedList p = fold <$> optional go
	where
	go = token' PParenL *> go' <* token' PParenR
	go' = p `Parsec.sepEndBy` token' PComma

token :: MonadParsec Error Stream m => String -> Prism' Token a -> m a
token name prism = Parsec.token (go . snd) Nothing
	where
	go = maybe (Left (Nothing, Set.singleton error)) Right . preview prism
	error = Parsec.Label (NonEmpty.fromList name)

token' :: MonadParsec Error Stream m => Token -> m ()
token' expected = void $ Parsec.token (go . snd) Nothing
	where
	go token
		| token == expected = Right token
		| otherwise = Left (Nothing, Set.singleton error)

	error = Parsec.Tokens ((Pos 0 0, expected) :| [])
