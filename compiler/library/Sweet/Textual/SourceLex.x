{
{-# LANGUAGE NoStrictData #-}

module Sweet.Textual.SourceLex
	( lexSource
	) where

import Data.ByteString.Lazy (ByteString)
import Data.Set (Set)
import Data.Text (Text)
import Data.Text.Encoding (decodeUtf8)

import qualified Data.ByteString.Lazy as LBS
import qualified Data.Set as Set
import qualified Data.Text as Text

import Sweet.Textual.Token (Pos (..), Token (..))
}

%wrapper "posn-bytestring"

token :-
	[\ \t\f\v\r\n] ;
	\/\/.* ;

	\#else { token $ \_ -> HElse }
	\#endif { token $ \_ -> HEndif }
	\#error { token $ \_ -> HError }
	\#ifdef { token $ \_ -> HIfdef }
	\#warning { token $ \_ -> HWarning }

	do { token $ \_ -> KDo }
	domain { token $ \_ -> KDomain }
	foreign { token $ \_ -> KForeign }
	func { token $ \_ -> KFunc }
	let { token $ \_ -> KLet }
	match { token $ \_ -> KMatch }
	namespace { token $ \_ -> KNamespace }
	operator { token $ \_ -> KOperator }
	unwrap { token $ \_ -> KUnwrap }
	using { token $ \_ -> KUsing }
	wrap { token $ \_ -> KWrap }

	\<\> { token $ \_ -> PLessGreater }
	\:\: { token $ \_ -> PColonColon }

	\- { token $ \_ -> PMinus }
	\, { token $ \_ -> PComma }
	\; { token $ \_ -> PSemicolon }
	\! { token $ \_ -> PBang }
	\. { token $ \_ -> PPeriod }
	\( { token $ \_ -> PParenL }
	\) { token $ \_ -> PParenR }
	\[ { token $ \_ -> PBracketL }
	\] { token $ \_ -> PBracketR }
	\{ { token $ \_ -> PBraceL }
	\} { token $ \_ -> PBraceR }
	\* { token $ \_ -> PAsterisk }
	\/ { token $ \_ -> PSlash }
	\# { token $ \_ -> PHash }
	\% { token $ \_ -> PPercent }
	\+ { token $ \_ -> PPlus }
	\< { token $ \_ -> PLess }
	\= { token $ \_ -> PEquals }
	\> { token $ \_ -> PGreater }
	\| { token $ \_ -> PPipe }
	\$ { token $ \_ -> PDollar }

	[a-zA-Z][a-zA-Z0-9]* { token $ Identifier . decodeUtf8 . LBS.toStrict }

	-- TODO: Handle escape sequences.
	\'[~\']*\' { token $ USVLiteral . Text.head . Text.tail . decodeUtf8 . LBS.toStrict }
	\"[~\"]*\" { token $ TextLiteral . Text.init . Text.tail . decodeUtf8 . LBS.toStrict }

{
token :: (ByteString -> a) -> AlexPosn -> ByteString -> (Pos, a)
token f (AlexPn _ line col) b = (Pos line col, f b)

-- | Lex source text, given a set of defines. The defines are those that may
-- appear in @#ifdef@ conditions.
lexSource :: Set Text -> ByteString -> [(Pos, Token)]
lexSource defines = go 0 . alexScanTokens
	where
	go :: Word -> [(Pos, Token)] -> [(Pos, Token)]

	go n ((_, HIfdef) : (_, Identifier option) : toks)
		| option `Set.member` defines = go (n + 1) toks
		| otherwise = go n (skipUntilEndif 0 toks)
	go _ ((_, HIfdef) : _) = error "lexSource: ill-formed #ifdef"

	go 0 ((_, HEndif) : _) = error "lexSource: stray #endif"
	go n ((_, HEndif) : toks) = go (n - 1) toks

	go n (tok : toks) = tok : go n toks
	go 0 [] = []
	go _ [] = error "lexSource: missing #endif"

	skipUntilEndif :: Word -> [(Pos, Token)] -> [(Pos, Token)]

	skipUntilEndif n ((_, HIfdef) : (_, Identifier _) : toks) = skipUntilEndif (n + 1) toks
	skipUntilEndif _ ((_, HIfdef) : _) = error "lexSource: ill-formed #ifdef"

	skipUntilEndif 0 ((_, HEndif) : toks) = toks
	skipUntilEndif n ((_, HEndif) : toks) = skipUntilEndif (n - 1) toks

	skipUntilEndif n (_ : toks) = skipUntilEndif n toks
	skipUntilEndif _ [] = error "lexSource: missing #endif"
}
