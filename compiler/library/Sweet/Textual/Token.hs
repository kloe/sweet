{-# LANGUAGE TemplateHaskell #-}

module Sweet.Textual.Token where

import Control.Lens (makePrisms)
import Data.Text (Text)

data Pos = Pos
	{ posLine :: {-# UNPACK #-} Int
	, posColumn :: {-# UNPACK #-} Int }
	deriving (Eq, Ord, Show)

data Token :: * where

	HElse :: Token
	HEndif :: Token
	HError :: Token
	HIfdef :: Token
	HWarning :: Token

	KDo :: Token
	KDomain :: Token
	KForeign :: Token
	KFunc :: Token
	KLet :: Token
	KMatch :: Token
	KNamespace :: Token
	KOperator :: Token
	KUnwrap :: Token
	KUsing :: Token
	KWrap :: Token

	PColonColon :: Token
	PLessGreater :: Token

	PAsterisk :: Token
	PBang :: Token
	PBraceL :: Token
	PBraceR :: Token
	PBracketL :: Token
	PBracketR :: Token
	PComma :: Token
	PDollar :: Token
	PEquals :: Token
	PGreater :: Token
	PHash :: Token
	PLess :: Token
	PMinus :: Token
	PParenL :: Token
	PParenR :: Token
	PPercent :: Token
	PPeriod :: Token
	PPipe :: Token
	PPlus :: Token
	PSemicolon :: Token
	PSlash :: Token

	Identifier :: Text -> Token

	USVLiteral :: Char -> Token
	TextLiteral :: Text -> Token

deriving stock instance Eq Token
deriving stock instance Ord Token
deriving stock instance Show Token

$(makePrisms ''Token)
