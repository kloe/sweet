-- | Implementation of the 32-bit FNV-1a hash algorithm.
module Data.Hash.FNV1a
	( fnv1a
	) where

import Data.Bits (xor)
import Data.Foldable (foldl')
import Data.Word (Word32)

fnvOffsetBasis, fnvPrime :: Word32
fnvOffsetBasis = 2166136261
fnvPrime = 16777619
{-# INLINE fnvOffsetBasis #-}
{-# INLINE fnvPrime #-}

fnv1a :: (Foldable f, Integral b) => f b -> Word32
fnv1a = foldl' (\h b -> (h `xor` fromIntegral b) * fnvPrime) fnvOffsetBasis
{-# INLINE fnv1a #-}
