module Main
	( main
	) where

import Control.Applicative ((<**>), many)
import Data.Foldable (fold)

import qualified Data.ByteString.Lazy as LBS
import qualified Options.Applicative as O

data Options = Options
	{ optionsArtifact :: String
	, optionsObjects :: [String] }
	deriving stock (Show)

optionParser :: O.ParserInfo Options
optionParser = O.info (parser <**> O.helper) info
	where
	parser = do
		artifact <- O.strOption $ fold
			[ O.long "artifact"
			, O.metavar "ARTIFACT"
			, O.help "Artifact to generate" ]
		objects <- many . O.argument O.str $ fold
			[ O.metavar "OBJECTS ..."
			, O.help "Object files to link" ]
		pure $ Options artifact objects

	info = fold
		[ O.fullDesc
		, O.progDesc "Linker for the Sweet programming language" ]

main :: IO ()
main = do
	options <- O.execParser optionParser
	objects <- traverse LBS.readFile (optionsObjects options)
	let artifact = "\"use strict\";\n" <> fold objects
	LBS.writeFile (optionsArtifact options) artifact
