let
	nixpkgs = import ../nix/nixpkgs.nix;
	ghc = import ./nix/ghc.nix;
in
	nixpkgs.haskell.packages.${ghc}.mkDerivation {
		pname = "sweetc";
		version = "0.0.0";
		license = nixpkgs.stdenv.lib.licenses.agpl3;
		src = ./.;
		buildDepends =
			let h = nixpkgs.haskell.packages.${ghc}; in
			import ./nix/dependencies.nix h;
	}
